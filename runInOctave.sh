#!/bin/bash
#path to this script
rootpath="$( cd "$( dirname "$0" )" && pwd )" 
#relative path to the folder of .m script Select_Ground_Motions
relpath="/Matlab_selection/conditional_selection/"
#join paths
relpath1=$rootpath$relpath
#relative path to mvnrnd.m function
relpath2=$relpath1"statistics/inst/"
#string of octave commands:
#1.command: addpath to mvnrnd.m function
#2.command: run .m script Select_Ground_Motions 
octstr="addpath('$relpath2');Select_Ground_Motions"

#chane dir and cal octave
cd $relpath1 && octave --eval $octstr
