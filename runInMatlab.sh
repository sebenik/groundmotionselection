#!/bin/bash
#path to this script
rootpath="$( cd "$( dirname "$0" )" && pwd )" 
#relative path to the folder of .m script Select_Ground_Motions
relpath='/Matlab_selection/conditional_selection/'
#join paths
relpath=$rootpath$relpath
#path to matlab or alias
matlabpath='matlab'
#string to run matlab with arguments
matstr="$matlabpath -nodesktop -nosplash -r "
#add command to sting that runs .m script Select_Ground_Motions 
matstr+="Select_Ground_Motions"
#add quit function to string to close matlab
matstr+=";quit()"

#change dir and call matlab
cd $relpath && $matstr
