% Documentation of the workspace 'rec_selection_meta_data.mat'
%
% This workspace has been created based on the PEER NGA database ground
% motions (Chiou et al. 2008). The documentation of the variables provided
% here is based on the NGA Documentation file available at:
% http://peer.berkeley.edu/nga/documentation.html
%
% Not all the variables available in the workspace may be required for the
% ground-motion selection. The minimum requirements have been documented in
% Select_Ground_Motions.m
%
% Nirmal Jayaram, Ting Lin, Jack W. Baker
% Department of Civil and Environmental Engineering
% Stanford University
% Last Updated: 27 March 2010
%
% Referenced manuscripts:
%
% Chiou, B., R. Darragh, N. Gregor, and W. Silva (2008). NGA project 
% strong-motion database. Earthquake Spectra 24(1), 23�44.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 

% Filename_1        : Filename of the time history data file in direction 1
% Filename_2        : Filename of the time history data file in direction 2
% Filename_FN       : Filename of the fault normal time history data file
% Filename_FP       : Filename of the fault parallel time history data file
% Filename_vert     : Filename of the vertical time history data file
% Periods           : Periods at which spectral accelerations have been
%                     computed and stored in the workspace
% Sa_1              : Spectral acceleration in direction 1
% Sa_2              : Spectral acceleration in direction 2
% Sa_FN             : Spectral acceleration in fault normal direction
% Sa_FP             : Spectral acceleration in fault parallel direction
% Sa_vert           : Spectral acceleration in vertical direction
% distance_cambell  : Campbell distance
% distance_closest  : Closest distance to the ruptured area
% distance_epi      : Epicentral distance
% distance_hyp      : Hypocentral distance
% distance_jb       : Joyner-Boore distance
% lowest_usable_freq: Lowest usable frequency
% magnitude         : Magnitude
% Mechanism         : Fault mechanism
% soil_Vs30         : Vs30 value














