Osnovna skripta za račun je .m file Selec_Ground_Motions in
se nahaja v mapi ./Matlab_selection/conditional_selection,
skupaj z ostalimi funkcijami in data-sourci, ki jih potrebuje.

Folder statistics je dodan za delovanje skripte v programu
Octave oz. za dostop do funkcije mvnrnd.m.

Z vsako od priloženih bash skipt poženemo račun s programom
Octave ali Matlab. Input je 'zapečen' v sami skripti
Select_Ground_Motions, output pa je datoteka Output_File.dat
in ena slika, ostale so 'izkopljene'.

Make script executable:
	-----------------------
	chmod +x runInMatlab.sh
	chmod +x runInOctave.sh
	-----------------------

!!!Matlab skripta
	V bash skripti je namesto poti do matlaba uporabljen njegov alias 'matlab'
	
	add alias from terminal: 
	----------------------------------------
	cd /usr/local/bin/ 
	sudo ln -s /path-to-matlab/matlab matlab 
	----------------------------------------
	
	Če alias ni nastavljen na matlab, je potrebno popravit spremenljivko matlabpath:
	matlabpath=/path-to-matlab/matlab


!!Rezultati
	Rezultati programa Matlab in Octave so različni. Nisem še uspel preverit ali
	je temu vzrok dodana funkcija mvnrnd.m ali kaj drugega.
